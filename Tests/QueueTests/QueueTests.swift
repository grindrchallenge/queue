import XCTest
@testable import Queue

class QueueTests: XCTestCase {
    
    func testQueueItemCount() {
        var queue = Queue<Int>()
        queue.enqueue(1)
        queue.enqueue(2)
        queue.enqueue(3)
        
        XCTAssertEqual(queue.count, 3)
    }
    
    func testQueueOperaion() {
        var queue = Queue<Int>()
        queue.enqueue(1)
        let item = queue.dequeue()
        
        XCTAssertEqual(item!, 1)
    }
    
    func testMultipleDequeue() {
        var queue = Queue<Int>()
        queue.enqueue(1)
        _ = queue.dequeue()
        let secondItem = queue.dequeue()
        
        XCTAssertEqual(secondItem, nil)
    }
    
    func testDequeueAll() {
        var queue = Queue<Int>()
        queue.enqueue(1)
        queue.enqueue(2)
        queue.enqueue(3)
        queue.dequeueAll()
        
        XCTAssertEqual(queue.count, 0)
    }
    
    func testMultipleEnqueue() {
        var queue = Queue<Int>()
        queue.enqueue([1, 2, 3])
        
        XCTAssertEqual(queue.count, 3)
    }
    
    func testCustomClass() {
        class Custom {
            var value: Int
            
            init(value: Int) {
                self.value = value
            }
        }
        
        var queue = Queue<Custom>()
        let customItem = Custom(value: 1)
        queue.enqueue(customItem)
        let dequeuedItem = queue.dequeue()
        
        XCTAssertEqual(dequeuedItem!.value, 1)
    }


    static var allTests : [(String, (QueueTests) -> () throws -> Void)] {
        return [
            ("testQueueItemCount", testQueueItemCount),
            ("testQueueOperaion", testQueueOperaion),
            ("testMultipleDequeue", testMultipleDequeue),
            ("testDequeueAll", testDequeueAll),
            ("testMultipleEnqueue", testMultipleEnqueue),
            ("testCustomClass", testCustomClass),
        ]
    }
}
