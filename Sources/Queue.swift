//
//  Queue.swift
//  Queue
//
//  Created by Rohit Kotian on 1/20/17.
//  Copyright © 2016 Rope LLC. All rights reserved.
//

public struct Queue<T> {

    fileprivate var _items: [T]! = Array<T>()
    
    public init() {}
    
    public init(_ size: Int) {
        _items.reserveCapacity(size)
    }
    
    public mutating func enqueue(_ item: T) {
        _items.append(item)
    }
    
    public mutating func enqueue(_ items: [T]) {
        _items.append(contentsOf: items)
    }
    
    public mutating func dequeue() -> T? {
        if _items.count > 0 {
            return _items.remove(at: 0)
        }
        return nil
    }
    
    public mutating func dequeueAll() {
        _items.removeAll()
    }
    
    public var count: Int {
        return _items.count
    }
    
}
